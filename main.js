var express = require('express');
var http = require('http');
var bodyParser = require('body-parser');
var Keycloak = require('keycloak-connect');
var mongoose = require('mongoose');

mongoose.connect('mongodb://localhost:27017/ehiveservice');

var db = mongoose.connection;
db.on('error', function () {
	console.error('connection error');
});

db.once('open', function () {
	console.log('Connection with database succeeded.');
});

var app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
var keycloak = new Keycloak({  });
app.use( keycloak.middleware() );
app.listen(3005, function()
{
    console.log("RUNNING");
});

app.get('/api', keycloak.protect(), function(req, res)
{
    console.log("/api");
    res.status(200).send({"response":true});
});